#!/usr/bin/env bash

main() {
  trap 'on_exit' SIGTERM SIGINT SIGABRT

  install_requirements
  prepare_for_tailscale
  tailscaled --verbose=1 --state=mem: &
  tailscale set --auto-update
  tailscale up --authkey=${TAILSCALE_AUTH_KEY} --hostname="fly-${FLY_REGION}" --advertise-exit-node
  tailscale web --readonly --listen 0.0.0.0:9101 &

  sleep_days "${SLEEP_DAYS:-21}"
}

install_requirements() {
  apk update
  apk upgrade
  apk add -u iptables ip6tables tailscale
}

on_exit() {
  tailscale logout
  exit
}

prepare_for_tailscale() {
  modprobe xt_mark

  echo 'net.ipv4.ip_forward = 1'          | tee -a /etc/sysctl.conf
  echo 'net.ipv6.conf.all.forwarding = 1' | tee -a /etc/sysctl.conf
  sysctl -p /etc/sysctl.conf

  iptables  -t nat -A POSTROUTING -o eth0 -j MASQUERADE
  ip6tables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
}

sleep_days() {
  local DAYS="${1:?Specify the number of days to sleep}"
  UNTIL="$(echo "$EPOCHSECONDS + 60 * 60 * 24 * ${DAYS} / 1" | bc)"

  echo "Sleeping for ${DAYS} days"
  while [[ "${EPOCHSECONDS}" -le "${UNTIL}" ]]; do
    sleep 3
  done
}

main
