FROM alpine:latest

RUN \
  sed -i.bak -E -e 's_/v[^/]+/_/edge/_' /etc/apk/repositories && \
  apk update && \
  apk upgrade && \
  apk add -u bash iptables ip6tables tailscale
USER root
WORKDIR /app
COPY ./start.sh /app/start.sh
CMD ["/app/start.sh"]
